# <challenge-name>

> short description of the challenge 

## Flag(s)

- <challenge-name>: `<flag>`

## Nextcloud Link

> Only relevant if the challenge contains an artifact 

## Proposed difficulty:

> Beginner, very easy, easy,..., very hard


## Prerequisites & Outcome

### Prerequisites

> list of things people should know e.g.

- Wireshark
- Basic scripting knowledge 

### Outcome

> list of things people should learn from this e.g. 

- How to find files send over the network using wireshark


## Solution(s)

> step by step solution to the challenge e.g.

### Challenge name (Name of subcallenge if more than one)

- Open wireshark and filter for HTTP post requests.
- Identify a POST request send to /login by looking in the info part
- Find the credentials send in the request by looking at the html-www-form-urlencoded  
  layer of the packet
- Use this to login to the website found on either the IP to which the request is going  
  or by finding the request URI in the HTTP layer of the packet
- See the flag in the top left

## How to run locally

> A few lines describing how people can start the challenge locally