from flask import Flask
import os
server = Flask(__name__)

@server.route("/")
def flag(FLAG = os.getenv('APP_FLAG')):
    return FLAG

if __name__ == "__main__":
   server.run(host='0.0.0.0') 